# prepscholar-viewer
A CLI scraper for information about colleges

`prepscholar-viewer` lets you scrape basic information regarding colleges from [PrepScholar](https://prepscholar.com).

## License
`prepscholar-viewer` is licensed under the MIT License.
