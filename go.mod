module prepscholar

go 1.18

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/fatih/color v1.13.0
	github.com/olekukonko/tablewriter v0.0.5
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/rivo/uniseg v0.4.2 // indirect
	golang.org/x/net v0.0.0-20220909164309-bea034e7d591 // indirect
	golang.org/x/sys v0.0.0-20220915200043-7b5979e65e41 // indirect
)
