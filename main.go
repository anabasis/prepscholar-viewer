package main

import (
	"bufio"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/fatih/color"
	"github.com/olekukonko/tablewriter"
)

var (
	bold       *color.Color = color.New(color.Bold)
	boldItalic *color.Color = color.New(color.Bold, color.Italic)
	boldRed    *color.Color = color.New(color.Bold, color.FgHiRed)
	boldCyan   *color.Color = color.New(color.Bold, color.FgHiCyan)
)

const userAgent = "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0"

func main() {
	var input string
	if len(os.Args) > 1 {
		input = strings.Join(os.Args[1:], " ")
	} else {
		bold.Printf("Enter college/university: ")
		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()
		input = scanner.Text()
		fmt.Println("")
	}

	getInfo(input)
}

func getInfo(college string) {
	college = strings.ToLower(strings.ReplaceAll(college, " ", "-"))

	client := &http.Client{}

	req, err := http.NewRequest(http.MethodGet, "https://www.prepscholar.com/sat/s/colleges/"+url.PathEscape(college)+"-admission-requirements", nil)
	if err != nil {
		log.Println(err)
	}

	req.Header.Set("User-Agent", userAgent)
	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		boldRed.Println("Invalid college name!")
		os.Exit(0)
	}

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	title := doc.FindMatcher(goquery.Single("title")).Text()
	boldCyan.Println(title)

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Statistic", "Value"})

	doc.Find("header.heading-head").Each(func(i int, s *goquery.Selection) {
		if i > 3 {
			return
		}
		table.Append(strings.Split(s.Find("h2").Text(), ": "))
	})
	table.SetBorder(false)
	table.SetTablePadding("\t")
	table.SetColumnSeparator(":")
	table.SetNoWhiteSpace(true)
	table.SetHeaderLine(false)
	table.SetColumnColor(tablewriter.Colors{int(color.Bold), int(color.Italic)}, tablewriter.Colors{})
	table.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
	table.Render()

	boldItalic.Println("Other Schools")
	doc.Find("table.table.table-school.sortable").Each(func(_ int, s *goquery.Selection) {
		table := tablewriter.NewWriter(os.Stdout)

		bold.Println(s.Prev().Find("h3").Text())
		s.Find("tr").Each(func(_ int, s *goquery.Selection) {
			var headerRow []string
			s.Find("th").Each(func(_ int, s *goquery.Selection) {
				title := s.Text()
				headerRow = append(headerRow, title)
			})
			table.SetHeader(headerRow)
			var row []string
			s.Find("td").Each(func(_ int, s *goquery.Selection) {
				title := s.Text()
				row = append(row, title)
			})
			table.Append(row)
		})

		table.SetBorder(false)
		table.SetRowSeparator("─")
		table.SetCenterSeparator("┼")
		table.SetColumnSeparator("│")
		table.Render()
	})
}
